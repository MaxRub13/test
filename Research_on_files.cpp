// Research_on_files.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "Research_on_files.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_RESEARCHONFILES, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_RESEARCHONFILES));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_RESEARCHONFILES));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_RESEARCHONFILES);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 900, 500, nullptr, nullptr, hInstance, nullptr);
   HWND hBut = CreateWindowW((LPCWSTR)"BUTTON", 0, WS_CHILD | WS_VISIBLE, 70, 70, 80, 25, hWnd, nullptr, hInst, 0);
   HWND hEdt = CreateWindowW((LPCWSTR)"EDIT", 0, WS_BORDER | WS_CHILD | WS_VISIBLE, 56, 10, 50, 18, hWnd, 0, hInst, 0);
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    HDC hDeviceContext;
    PAINTSTRUCT paintStruct;
    RECT rectPlace = { 201, 51, 489, 69 };
    HFONT hFont;
    HBRUSH hBrush;// = (HBRUSH)GetStockObject(BLACK_BRUSH);

    static PTCHAR text;
    static int size = 0;

    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_CREATE:
        text = (PTCHAR)GlobalAlloc(GPTR, 50000*sizeof(TCHAR));
        break;
    case WM_KEYDOWN:
        switch (wParam)
        {
        case VK_HOME:case VK_END: case VK_PRIOR:
        case VK_NEXT:case VK_LEFT: case VK_RIGHT:
        case VK_UP:case VK_DOWN:case VK_DELETE:
        case VK_SHIFT:/*case VK_SPACE:*/case VK_CONTROL:
        case VK_CAPITAL:case VK_MENU:case VK_TAB:
            break;
        case VK_BACK:
            size--;
            break;
        case VK_RETURN:
            size = 0;
            break;
        default:
            text[size] = (char)wParam;
            size++;         
            break;
        }
        InvalidateRect(hWnd, nullptr, TRUE);
        break;
    //all update here in WM_PAINT
    case WM_PAINT:
    {

        hDeviceContext = BeginPaint(hWnd, &paintStruct);
        //................................................
        POINT pt[5] = { {90, 200}, {270, 200}, {270, 300}, {90, 300}, {90, 200} };
        /*MoveToEx(hDeviceContext, pt[0].x, pt[0].y, nullptr);
        for (int i = 0; i < 5; ++i)
            LineTo(hDeviceContext, pt[i].x, pt[i].y);*/
        Polyline(hDeviceContext, pt, 5);
        //.................................................
            //..............................................
            TCHAR Find_what[] = _T("Find what :");
            TCHAR Replace_with[] = _T("Replace with :");
            TCHAR Filters[] = _T("Filters :");
            TCHAR Directory[] = _T("Directory :");
            //..............................................
            TextOut(hDeviceContext, 100, 50,
                    Find_what, _tcslen(Find_what));
            TextOut(hDeviceContext, 100, 80,
                    Replace_with, _tcslen(Replace_with));
            TextOut(hDeviceContext, 100, 110,
                    Filters, _tcslen(Filters));
            TextOut(hDeviceContext, 100, 140,
                    Directory, _tcslen(Directory));
            //..............................................
            Rectangle(hDeviceContext, 200, 50, 490, 70);
            Rectangle(hDeviceContext, 200, 80, 490, 100);
            Rectangle(hDeviceContext, 200, 110, 490, 130);
            Rectangle(hDeviceContext, 200, 140, 490, 160);
            //..............................................
            //GetClientRect(hWnd, &rectPlace);
            //DrawFrameControl(hDeviceContext, &rectPlace, DFC_BUTTON, DFCS_BUTTONCHECK);
            SetTextColor(hDeviceContext, NULL);
            hFont = CreateFont(20, 0, 0, 0, 0, 0, 0, 0,
                               DEFAULT_CHARSET,
                               0, 0, 0, 0,
                               L"Arial Bold");
            SelectObject(hDeviceContext, hFont);
            if(wParam != VK_RETURN)
                DrawText(hDeviceContext, (LPCWSTR)text, size, &rectPlace, DT_SINGLELINE | /*DT_CENTER | */ DT_VCENTER | DT_END_ELLIPSIS);
            //SelectObject(hDeviceContext, hBrush);
            EndPaint(hWnd, &paintStruct);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
