#pragma once

#include "resource.h"
#include <commdlg.h>
#include <commctrl.h>
#include <fstream>
#include <vector>
#include <string>
#include <filesystem>


#define MAX_LOADSTRING 100

HWND hFindDialog, hResultsDialog, hProgressDialog, hTreeResults;
BOOL MyFindFunction(HWND hGlg, HWND hResults, HWND hProgress);
int myFindFunctionStr(const std::string& line, const std::string& find_str, int pos);
void CharToString(std::string&, char*, int);

LPCWSTR StringToLPCWSTR(std::string&);
static TCHAR TCFindWhat[256] = _T("");;
static TCHAR TCFilters[256] = _T("");;
static TCHAR TCDirectory[256] = _T("");;
static TCHAR TCReplaceWith[256] = _T("");;
