// Find_in_file.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "Find_in_file.h"

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
const int LineHeight = 16;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    FindProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    ResultProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_FINDINFILE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FINDINFILE));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            if (!IsDialogMessage(hFindDialog, &msg))
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
    }

    return (int) msg.wParam;
}
//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW ;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FINDINFILE));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_FINDINFILE);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}
//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 750, 500, nullptr, nullptr, hInstance, nullptr);
   //HWND hWnd = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOGBAR), NULL, (DLGPROC)WndProc);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}
//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;
    static TCHAR name[256] = _T("");
    static OPENFILENAME file;
    std::ifstream in;
    std::ofstream out;
    static std::vector<std::string> v;
    std::vector<std::string>::iterator it;
    std::string st;
    int y, k;
    static int n, length, sx, sy, cx, iVscrollPos, iHscrollPos, COUNT, MAX_WIDTH;
    static SIZE size = { 8, 16 };

    switch (message)
    {
    case WM_CREATE:
        //create buttons
        file.lStructSize = sizeof(OPENFILENAME);
        file.hInstance = hInst;
        file.lpstrFilter = _T("Text\0*.txt");
        file.lpstrFile = name;
        file.nMaxFile = 256;
        file.lpstrInitialDir = _T(".\\");
        file.lpstrDefExt = _T("txt");
        return 0;
    case WM_VSCROLL:
        switch (LOWORD(wParam))
        {
        case SB_LINEUP: 
            iVscrollPos--;
            break;
        case SB_LINEDOWN: 
            iVscrollPos++; 
            break;
        case SB_PAGEUP: 
            iVscrollPos -= sy / size.cy; 
            break;
        case SB_PAGEDOWN: 
            iVscrollPos += sy / size.cy; 
            break;
        case SB_THUMBPOSITION: 
            iVscrollPos = HIWORD(wParam); 
            break;
        }
        iVscrollPos = max(0, min(iVscrollPos, COUNT));
        if (iVscrollPos != GetScrollPos(hWnd, SB_VERT))
        {
            SetScrollPos(hWnd, SB_VERT, iVscrollPos, TRUE);
            InvalidateRect(hWnd, NULL, TRUE);
        }
        break;
    case WM_HSCROLL:
        switch (LOWORD(wParam))
        {
        case SB_LINEUP: 
            iHscrollPos--; 
            break;
        case SB_LINEDOWN: 
            iHscrollPos++;
            break;
        case SB_PAGEUP: 
            iHscrollPos -= 8; 
            break;
        case SB_PAGEDOWN: 
            iHscrollPos += 8; 
            break;
        case SB_THUMBPOSITION: 
            iHscrollPos = HIWORD(wParam); 
            break;
        }
        iHscrollPos = max(0, min(iHscrollPos, MAX_WIDTH));
        if (iHscrollPos != GetScrollPos(hWnd, SB_HORZ))
        {
            SetScrollPos(hWnd, SB_HORZ, iHscrollPos, TRUE);
            InvalidateRect(hWnd, NULL, TRUE);
        }
        break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case ID_FILE_NEW:
                if (!v.empty()) 
                    std::vector<std::string>().swap(v);
                n = length = 0;
                SendMessage(hWnd, WM_SIZE, 0, sy << 16 | sx);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case ID_FILE_OPEN:
                file.Flags = OFN_HIDEREADONLY;
                if (!GetOpenFileName(&file)) 
                    return 1;
                in.open(name);
                while (getline(in, st))
                {
                    if (length < st.length()) 
                        length = st.length();
                    v.push_back(st);
                }
                in.close();
                n = v.size();
                SendMessage(hWnd, WM_SIZE, 0, sy << 16 | sx);
                InvalidateRect(hWnd, NULL, TRUE);
                break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case ID_FIND_FINDINFILES:
                hFindDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOGBAR), hWnd, FindProc);
                ShowWindow(hFindDialog, SW_SHOW);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_SIZE:
        sx = LOWORD(lParam);
        sy = HIWORD(lParam);
        k = n - sy / size.cy;
        if (k > 0) 
            COUNT = k; 
        else 
            COUNT = iVscrollPos = 0;
        SetScrollRange(hWnd, SB_VERT, 0, COUNT, FALSE);
        SetScrollPos(hWnd, SB_VERT, iVscrollPos, TRUE);
        k = length - sx / size.cx;
        if (k > 0)
            MAX_WIDTH = k; 
        else 
            MAX_WIDTH = iHscrollPos = 0;
        SetScrollRange(hWnd, SB_HORZ, 0, MAX_WIDTH, FALSE);
        SetScrollPos(hWnd, SB_HORZ, iHscrollPos, TRUE);
        break;
    case WM_PAINT:
        {
            hdc = BeginPaint(hWnd, &ps);
            for (y = 0, it = v.begin() + iVscrollPos; it != v.end() && y < sy; ++it, y += size.cy)
                if (iHscrollPos < it->length())
                    TabbedTextOutA(hdc, 0, y, it->data() + iHscrollPos, it->length() -
                        iHscrollPos, 0, NULL, 0);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
INT_PTR CALLBACK FindProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        int wmId = LOWORD(wParam);
        switch (wmId)
        {
        case IDCANCEL:
            SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_SETPOS, 0, 0);
        case IDC_BUTTON3:
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
            break;
        case IDC_FINDALL:
            hResultsDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hDlg, ResultProc);
            hProgressDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG2), hDlg, FindProc);
            if (!MyFindFunction(hDlg, hResultsDialog, hProgressDialog))
                return 1;
            SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_SETRANGE, 0, 1);
            break;
        case IDC_BUTTON2:
            MessageBox(hDlg, L"You pressed Replace in Files button", L"MessageBox", MB_OK | MB_ICONWARNING);
            break;
        case IDC_BUTTON4:
            static TCHAR dir[] = L"C:\\";
            DlgDirListComboBox(hDlg, (LPWSTR)dir, IDC_COMBO4, NULL, NULL);
            //DlgDirSelectComboBoxEx(hDlg,)
            MessageBox(hDlg, L"You pressed Directory button", L"MessageBox", MB_OK | MB_ICONWARNING);
            break;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
INT_PTR CALLBACK ResultProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
BOOL MyFindFunction(HWND hDlg, HWND hResults, HWND hProgress)
{
    using namespace std::filesystem;				//pay attention C++17 Standart
    std::ifstream fin;
    static TCHAR data[256] = _T("");
    using std::string;
    std::vector<string> pvFilters_str;		//vector for store filters
    std::vector<string> pvPath_str;        //for store path to files
    double countForMaximumProgressBar = 0;
    string strFindWhat;
    string strFilters;
    string strDirectory;
    //geting data for research 
    GetDlgItemText(hDlg, IDC_COMBO1, data, 256);
    CharToString(strFindWhat, (char*)data, 256);
    GetDlgItemText(hDlg, IDC_COMBO3, data, 256);
    CharToString(strFilters, (char*)data, 256);
    GetDlgItemText(hDlg, IDC_COMBO4, data, 256);
    CharToString(strDirectory, (char*)data, 256);
    SetDlgItemText(hProgress, IDC_PROGRESS1, data);
    //starting searching
    //for current file only.............................................................
    if (IsDlgButtonChecked(hDlg, IDC_CHECK1))
    {
        if (!exists(strDirectory))                      //check file
        {
            MessageBox(hDlg, L"No existing file", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else
        {
            std::string line;
            int count_line = 0;
            fin.open(strDirectory);
            ShowWindow(hResults, SW_SHOW);
            TVITEM data = { 0 };
            data.pszText = (LPWSTR)strDirectory.c_str();
            TVITEM* pdata = &data;
            if (fin.is_open())
            {
                while (std::getline(fin, line))
                {
                    count_line++;
                    int count = myFindFunctionStr(line, strFindWhat, 0);
                    if (count > 0)
                    {
                        SendDlgItemMessage(
                            hResultsDialog,
                            IDC_TREE1,
                            TVM_SETITEM,
                            0,
                            (LPARAM)pdata);
                        InvalidateRect(hResults, NULL, TRUE);
                    }
                }
                SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_SETSTEP, (WPARAM)1, 0);
                count_line = 0;
                SendDlgItemMessage(hProgressDialog, IDC_PROGRESS1, PBM_STEPIT, 0, 0);
                ShowWindow(hProgressDialog, SW_SHOW);
            }
            else
                MessageBox(hDlg, L"Unable to open file.", L"Alert!", MB_OK | MB_ICONWARNING);
            fin.close();
            //EnableWindow(GetDlgItem(hProgressDialog, IDCANCEL), false);
            SendDlgItemMessage(hProgressDialog, IDCANCEL, BM_SETSTATE, TRUE, 0L);
        }
    }
    //.........................................................................	
    else if (IsDlgButtonChecked(hDlg, IDC_CHECK2))
    {
        if (!exists(strDirectory))                      //check directory
        {
            MessageBox(hDlg, L"No existing directory", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else
        {
            //filters string processing.................................................
            std::string temp;
            for (int i = 0; i < strFilters.size() || strFilters[i] != '\0'; i++)			//pay attention need '*' in filters			
            {
                if (strFilters[i] == '*')
                    continue;
                else if (strFilters[i] == ';' || strFilters[i] == ' ')
                {
                    if (temp.size() > 0)
                    {
                        pvFilters_str.push_back(temp);
                        temp.clear();
                    }
                    else
                        continue;
                }
                else
                    temp += strFilters[i];
            }
            pvFilters_str.push_back(temp);
            temp.clear();
        }
        for (const auto& entry : recursive_directory_iterator(strDirectory))
        {
            std::filesystem::path temp_fs_path = entry.path();
            for (int i = 0; i < pvFilters_str.size(); i++)
            {
                if (pvFilters_str[i] == temp_fs_path.extension())        //check files extension and save path in vector
                {
                    //here we need find size of files for progress Bar
                    countForMaximumProgressBar += std::filesystem::file_size(temp_fs_path);
                    std::string data = temp_fs_path.string();
                    pvPath_str.push_back(data);
                }
            }

        }
        if (pvPath_str.size() == 0)
        {
            MessageBox(hDlg, L"No files with this filters!", L"Alert!", MB_OK | MB_ICONWARNING);
        }
        else
        {
            std::string data;
            std::string line;
            int count_line = 0;
            ShowWindow(hProgress, SW_SHOW);
            //EnableWindow(GetDlgItem(hProgressDialog, IDCANCEL), true);
            for (int i = 0; i < pvPath_str.size(); i++)
            {
                data = pvPath_str[i];				//pay attention 
                fin.open(pvPath_str[i]);
                if (fin.is_open())
                {
                    while (std::getline(fin, line))
                    {
                        count_line++;
                        int count = myFindFunctionStr(line, strFindWhat, 0);
                        //if (count > 0)
                            //dataGridView1->Rows->Add(gcnew System::String(data.c_str()), gcnew System::String(line.c_str()), count);
                    }
                    count_line = 0;
                    DWORD sizeStep = (double)file_size(data) / (countForMaximumProgressBar / 100);//this version of the solution loses accuracy
                    SendDlgItemMessage(hProgress, IDC_PROGRESS1, PBM_SETSTEP, (WPARAM)sizeStep, 0);
                    SendDlgItemMessage(hProgress, IDC_PROGRESS1, PBM_STEPIT, 0, 0);
                    InvalidateRect(hProgress, NULL, TRUE);

                }
                else
                    MessageBox(hDlg, L"Unable to open file.", L"Alert!", MB_OK | MB_ICONWARNING);
                fin.close();
            }
            //EnableWindow(GetDlgItem(hProgressDialog, IDCANCEL), false);
            SendDlgItemMessage(hProgressDialog, IDCANCEL , BM_SETSTATE, TRUE, 0L);
        }
    }
    else if (IsDlgButtonChecked(hDlg, IDC_CHECK3))
    {
        MessageBox(hDlg, L"In hidden folders", L"Done!", MB_OK | MB_ICONWARNING);
    }
    return TRUE;
}
int myFindFunctionStr(const std::string& line, const std::string& find_str, int pos)
{
    int count = 0;
    int count_p = line.find(find_str, pos);
    if (count_p != std::string::npos)
    {
        if (count_p + find_str.size() <= line.size())
        {
            count++;
            return count + myFindFunctionStr(line, find_str, count_p + (find_str.size() - 1));
        }
        else
            return 0;
    }
    else
        return 0;
}
void CharToString(std::string& str, char* data, int n)
{
    for (int i = 0; i < n; i++)
        str += data + i;
}
LPCWSTR StringToLPCWSTR(std::string& str)
{
    std::wstring temp = std::wstring(str.begin(), str.end());
    LPCWSTR lpcwstr = temp.c_str();
    return lpcwstr;
}