//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Find_in_file.rc
//
#define IDC_MYICON                      2
#define IDD_FINDINFILE_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDD_DIALOGBAR                   104
#define IDM_EXIT                        105
#define IDI_FINDINFILE                  107
#define IDI_SMALL                       108
#define IDC_FINDINFILE                  109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_DIALOG2                     130
#define IDC_FINDALL                     1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_BUTTON4                     1003
#define IDC_COMBO1                      1004
#define IDC_COMBO2                      1005
#define IDC_COMBO3                      1006
#define IDC_COMBO4                      1007
#define IDC_CHECK1                      1008
#define IDC_CHECK2                      1009
#define IDC_CHECK3                      1010
#define IDC_CHECK4                      1011
#define IDC_CHECK5                      1012
#define IDC_BUTTON5                     1015
#define IDC_TREE1                       1017
#define IDC_PROGRESS1                   1018
#define ID_FIND_FINDINFILES             32775
#define ID_FIND_R                       32781
#define ID_FILE_FILE                    32782
#define ID_FILE_FILENEW                 32783
#define ID_FILE_OPEN                    32784
#define ID_FILE_NEW                     32785
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32786
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
