#include "MyForm.h"
using namespace System;
using namespace System::Windows::Forms;
[STAThreadAttribute]                    //pay attention, an entry point function,
int main() {
    Application::EnableVisualStyles();
    Application::SetCompatibleTextRenderingDefault(false);
    CppFindinfile::MyForm form;
    Application::Run(% form);
    return 0;
}